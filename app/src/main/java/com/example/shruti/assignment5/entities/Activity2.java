package com.example.shruti.assignment5.entities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.widget.TextView;

import com.example.shruti.assignment5.R;

import java.io.Serializable;
import java.util.ArrayList;



public class Activity2 extends ActionBarActivity {

    TextView firstName, lastName, gender, link;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2);
        firstName = (TextView) findViewById(R.id.fName_display);
        lastName = (TextView) findViewById(R.id.lName_display);
        gender = (TextView) findViewById(R.id.gender_display);
        link = (TextView) findViewById(R.id.link_display);

             intent=this.getIntent();
        if (intent != null) {
            final String receiveValue1 = intent.getStringExtra("name1");
            firstName.setText(String.valueOf(receiveValue1));
            final String receiveValue2 = intent.getStringExtra("name2");
            lastName.setText(receiveValue2);
            final String receiveValue3 = intent.getStringExtra("gender");
            gender.setText(String.valueOf(receiveValue3));
            final String receiveValue4 = intent.getStringExtra("link");
            link.setText(String.valueOf(receiveValue4));
        }
    }


}

