package com.example.shruti.assignment5.main;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.shruti.assignment5.R;
import com.example.shruti.assignment5.entities.Activity2;


import com.example.shruti.assignment5.entities.TestAdapter;


import java.util.ArrayList;


import com.example.shruti.assignment5.util.FacebookUtils;
import com.facebook.Session;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends ActionBarActivity {
    public static ArrayList<String> friendList1 = new ArrayList<>();
    public static ArrayList<String> listId = new ArrayList<String>();
    TestAdapter adapter;
    String token;
    String fName, lName, gender, link;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);


        ListView listView = (ListView) findViewById(R.id.list);

        adapter = new TestAdapter(friendList1, this);
        listView.setAdapter(adapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                AsyncHttpClient client = new AsyncHttpClient();
                String url="https://graph.facebook.com/" + listId.get(position)+"?access_token="+token;
                client.get(url, new AsyncHttpResponseHandler() {

                    @Override
                    public void onSuccess(String s) {
                       // super.onSuccess(s);

                        JSONObject jsonObj = null;
                        try {
                            jsonObj = new JSONObject(s);
                            fName = jsonObj.getString("first_name");
                            lName = jsonObj.getString("last_name");
                            gender = jsonObj.getString("gender");
                            link = jsonObj.getString("link");
                            Intent intent;
                            intent = new Intent(MainActivity.this, Activity2.class);
                            intent.putExtra("name1", fName);
                            intent.putExtra("name2", lName);
                            intent.putExtra("gender", gender);
                            intent.putExtra("link", link);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(Throwable throwable,String s) {
                        super.onFailure(throwable,s);
                    }
                });


            }
        });

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode,
                resultCode, data);
    }


    public void getFriend(String s) {
        token = s;
        AsyncHttpClient client = new AsyncHttpClient();
        client.get("https://graph.facebook.com/me/friends?access_token=" + token, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                String jsonStr = s;
                JSONObject jsonObj = null;
                try {
                    jsonObj = new JSONObject(jsonStr);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                // Getting JSON Array node
                JSONArray friends = null;
                try {
                    friends = jsonObj.getJSONArray("data");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                adapter.friendList1.clear();
                // looping through All Contacts
                for (int i = 0; i < friends.length(); i++) {
                    JSONObject c = null;
                    try {
                        c = friends.getJSONObject(i);

                        String name = c.getString("name");
                        String id=c.getString("id");
                        adapter.friendList1.add(name);
                        listId.add(id);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.show_btn:

                FacebookUtils.fbLogin(0, MainActivity.this);
        }

    }

}








