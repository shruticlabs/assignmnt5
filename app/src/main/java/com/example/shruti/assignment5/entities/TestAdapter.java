package com.example.shruti.assignment5.entities;
import com.example.shruti.assignment5.main.*;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.shruti.assignment5.R;



import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static com.example.shruti.assignment5.main.MainActivity.friendList1;

public class TestAdapter extends BaseAdapter {

 public ArrayList<String> friendList1;

    Context ctx;
  //  TextView tv;

    public TestAdapter(ArrayList<String> friendList, Context ctx) {
        this.friendList1 = friendList;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return friendList1.size();
    }

    @Override
    public Object getItem(int position) {
        return friendList1.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)ctx.getSystemService(ctx.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.single_row,null);
        TextView viewName = (TextView)view.findViewById(R.id.user_name);


        viewName.setText(friendList1.get(position));
        return view;
    }
}
